/**
 * *************************************************************************
 * *                   Drag & Dop Fill in the blank                       **
 * *************************************************************************
 * @package     mod                                                       **
 * @subpackage  ddfill                                                    **
 * @name        ddfill                                                    **
 * @copyright   Oohoo It Services Inc.                                    **
 * @link        http://www.csj.ualberta.ca                                **
 * @author      Patrick Thibaudeau                                        **
 * @author      Nicolas Bretin                                            **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
$('#return').button();

function init() {
    $('#ddfill_sentences').dataTable();
    $('#create_sentence').button();
    $('#preview_quiz').button();
}

function initQuiz(id, words, answer, qid) {
    //id: Int Used for identifying divs and other objects
    //words: Array List of available words
    //answer: String Answer to the question
    //qid: Int Question ID from sentences table
    //Find out how many words are in the list
    var word_count = words.length;
    //sort words
    words.sort(function() {
        return Math.random() - .5
    });
    //Create the word divs and populate the possible-answers div
    for (var i = 1; i <= word_count; i++) {
        
        $("<div id='select" + id + "'  class='words'" + id + ">" + words[i - 1] + "</div>").data('word' + id, words[i - 1]).appendTo('#possible-answers' + id).draggable({
            containment: '#sentences_wrapper' + id,
            stack: '#word' + id + ' div',
            cursor: 'move',
            revert: true
        });
    }
    //Make the answer div accept dropped items
    $("#answer" + id).data('word' + id, answer).droppable({
        accept: '#sentences_wrapper' + id + ' div',
        hoverClass: 'hovered',
        drop: handleWordDrop
    });

    //Function that accepts the drops and populates the score area and the correct and wrong answers
    function handleWordDrop(event, ui) {
        //Gather/Create are variables
        var word = $(this).data('word' + id);
        var answer = ui.draggable.data('word' + id);
        var count = $('#count').val();
        var attempts = '';

        //Is the word that is dropped equal to the answer
        if (word == answer) {
            //Remove the dragged word block
            ui.draggable.remove();
            //Change the original answer div to the correct answer
            $("#answer" + id).addClass('correct');
            $("#answer" + id).html(word);

            //Update the score board
            attempts = $('#attempts').val();
            attempts = Number(attempts) + 1;
            $('#attempts').val(attempts);
            var total = attempts + "/" + count;
            $('#total').html(total);
            $('#possible-answers' + id).remove();
        } else {
            //Wrong word was used. Change to an error color and shake 
            //the block showing it the wrong answer.
            $(ui.draggable).addClass('ui-state-error');
            $(ui.draggable).effect('shake');

            //Update the score board
            attempts = $('#attempts').val();
            attempts = Number(attempts) + 1;
            $('#attempts').val(attempts);
            var total = attempts + "/" + count;
            $('#total').html(total);
        }
    }
}


