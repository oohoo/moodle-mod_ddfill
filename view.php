<?php
/**
 * *************************************************************************
 * *                   Drag & Dop Fill in the blank                       **
 * *************************************************************************
 * @package     mod                                                       **
 * @subpackage  ddfill                                                    **
 * @name        ddfill                                                    **
 * @copyright   Oohoo It Services Inc.                                    **
 * @link        http://www.csj.ualberta.ca                                **
 * @author      Patrick Thibaudeau                                        **
 * @author      Nicolas Bretin                                            **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

$id = optional_param('id', 0, PARAM_INT); // course_module ID, or
$n  = optional_param('n', 0, PARAM_INT);  // ddfill instance ID - it should be named as the first character of the module

if ($id) {
    $cm         = get_coursemodule_from_id('ddfill', $id, 0, false, MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $ddfill  = $DB->get_record('ddfill', array('id' => $cm->instance), '*', MUST_EXIST);
} elseif ($n) {
    $ddfill  = $DB->get_record('ddfill', array('id' => $n), '*', MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $ddfill->course), '*', MUST_EXIST);
    $cm         = get_coursemodule_from_instance('ddfill', $ddfill->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);
$context = context_module::instance($cm->id);

add_to_log($course->id, 'ddfill', 'view', "view.php?id={$cm->id}", $ddfill->name, $cm->id);

if (has_capability('mod/ddfill:edit', $context)) {
    redirect('sentences.php?cmid='. $cm->id);
} else {
    redirect('quiz.php?cmid='. $cm->id);
}

