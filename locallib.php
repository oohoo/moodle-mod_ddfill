<?php
/**
 * *************************************************************************
 * *                   Drag & Dop Fill in the blank                       **
 * *************************************************************************
 * @package     mod                                                       **
 * @subpackage  ddfill                                                    **
 * @name        ddfill                                                    **
 * @copyright   Oohoo It Services Inc.                                    **
 * @link        http://www.csj.ualberta.ca                                **
 * @author      Patrick Thibaudeau                                        **
 * @author      Nicolas Bretin                                            **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
defined('MOODLE_INTERNAL') || die();
include_once('config.php');

function ddfill_page($url, $pagetitle, $pageheading, $context, $pagelayout = 'admin') {
    global $CFG, $PAGE;

    $PAGE->set_url($CFG->pluginlocalwww . $url);
    $PAGE->set_title($pagetitle);
    $PAGE->set_heading($pageheading);
    $PAGE->set_pagelayout($pagelayout);
    $PAGE->set_context($context);
    $PAGE->requires->jquery();
    //$PAGE->requires->jquery_plugin('migrate');
    $PAGE->requires->jquery_plugin('ui');
    $PAGE->requires->jquery_plugin('ui-css');
    $PAGE->requires->css($CFG->pluginlocalstyle . "ddfill.css");
    $PAGE->requires->css($CFG->pluginlocalscript . "dataTables/css/jquery.dataTables.css");
    $PAGE->requires->js($CFG->pluginlocalscript . "dataTables/js/jquery.dataTables.min.js");
    $PAGE->requires->js($CFG->pluginlocalscript . "ddfill.js");
    $PAGE->requires->js($CFG->pluginlocalscript . "jquery.ui.touch-punch.min.js");
    $PAGE->requires->js($CFG->pluginlocalscript . "jquery-scrolltofixed.js");
}

function ddfill_delete_button($url, $urlparams, $elemName) {
    global $OUTPUT;

    return $OUTPUT->action_icon(
                    new moodle_url($url, $urlparams), new pix_icon('t/delete', ''), new confirm_action(get_string('confirm_delete', 'ddfill', $elemName)), null, get_string('btn_delete', 'ddfill'));
}
