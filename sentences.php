<?php

/**
 * *************************************************************************
 * *                   Drag & Dop Fill in the blank                       **
 * *************************************************************************
 * @package     mod                                                       **
 * @subpackage  ddfill                                                    **
 * @name        ddfill                                                    **
 * @copyright   Oohoo It Services Inc.                                    **
 * @link        http://www.csj.ualberta.ca                                **
 * @author      Patrick Thibaudeau                                        **
 * @author      Nicolas Bretin                                            **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once('locallib.php');

/**
 * Display the content of the page
 * @global stdobject $CFG
 * @global moodle_database $DB
 * @global core_renderer $OUTPUT
 * @global moodle_page $PAGE
 * @global stdobject $SESSION
 * @global stdobject $USER
 */
function display_page() {
    // CHECK And PREPARE DATA
    global $CFG, $OUTPUT, $SESSION, $PAGE, $DB, $USER;

    $cmid = required_param('cmid', PARAM_INT);

    $cm = get_coursemodule_from_id('ddfill', $cmid);
    $course = $DB->get_record('course', array('id' => $cm->course));
    require_login($course, true, $cm);
    $context = context_module::instance($cm->id);

    require_capability('mod/ddfill:edit', $context);

    $sentences = $DB->get_records('ddfill_sentences', array('ddfillid' => $cm->instance));

    ddfill_page($CFG->pluginlocalwww . '/sentences.php', get_string('pluginname', 'ddfill'), get_string('configuration', 'ddfill'), $context);
    //--------------------------------------------------------------------------
    echo $OUTPUT->header();
    //**********************
    //*** DISPLAY HEADER ***
    $initjs = "$(document).ready(function() {
                        init();
                    });";
    echo html_writer::script($initjs);
    //Content
    $html = "<table id='ddfill_sentences'>"." \n";
    $html .= "<thead>"." \n";
    $html .= "	<tr>"." \n";
    $html .= "      <th>" . get_string('sentenceleft', 'ddfill') . "</th>"." \n";
    $html .= "      <th>" . get_string('answer', 'ddfill') . "</th>"." \n";
    $html .= "      <th>" . get_string('sentenceright', 'ddfill') . "</th>"." \n";
    $html .= "      <th>" . get_string('answerlist', 'ddfill') . "</th>"." \n";
    $html .= "      <th>" . get_string('actions', 'ddfill') . "</th>"." \n";
    $html .= "  </tr>"." \n";
    $html .= "</thead>"." \n";
    $html .= "<tbody>"." \n";
    foreach($sentences as $s) {
    $html .= "	<tr>"." \n";
    $html .= "      <td><a href='edit.php?cmid=$cmid&id=$s->id'>$s->sentenceleft</a></td>"." \n";
    $html .= "      <td>$s->answer</td>"." \n";
    $html .= "      <td>$s->sentenceright</td>"." \n";
    $html .= "      <td>$s->answerlist</td>"." \n";
    $html .= "      <td>" . ddfill_delete_button("delete.php" , array('id' => $s->id, 'cmid' => $cmid), 'delete') . "</td>"." \n";
    $html .= "	</tr>"." \n";
    }
    $html .= "</tbody>"." \n";
    $html .= "</table>"." \n";
    $html .= "<div id='btn_container'>"."\n";
    $html .= "<a href='edit.php?cmid=$cmid' id='create_sentence'>" . get_string('create_sentence', 'ddfill') . "</a>";
    $html .= "<a href='quiz.php?cmid=$cmid' id='preview_quiz'>" . get_string('preview_quiz', 'ddfill') . "</a>";
    $html .= "</div>";
    echo $html;
    //**********************
    //*** DISPLAY FOOTER ***
    //**********************
    echo $OUTPUT->footer();
}

display_page();

