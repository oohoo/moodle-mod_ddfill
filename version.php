 <?php
/**
 * *************************************************************************
 * *                   Drag & Dop Fill in the blank                       **
 * *************************************************************************
 * @package     mod                                                       **
 * @subpackage  ddfill                                                    **
 * @name        ddfill                                                    **
 * @copyright   Oohoo It Services Inc.                                    **
 * @link        http://www.csj.ualberta.ca                                **
 * @author      Patrick Thibaudeau                                        **
 * @author      Nicolas Bretin                                            **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */

defined('MOODLE_INTERNAL') || die();

$module->version   = 0;               // If version == 0 then module will not be installed
$module->version   = 2014020700;      // The current module version (Date: YYYYMMDDXX)
$module->requires  = 2010031900;      // Requires this Moodle version
$module->cron      = 0;               // Period for cron to check this module (secs)
$module->component = 'mod_ddfill'; // To check on upgrade, that module sits in correct place
$module->maturity = MATURITY_STABLE; // To check on upgrade, that module sits in correct place
