<?php

/**
 * *************************************************************************
 * *                   Drag & Dop Fill in the blank                       **
 * *************************************************************************
 * @package     mod                                                       **
 * @subpackage  ddfill                                                    **
 * @name        ddfill                                                    **
 * @copyright   Oohoo It Services Inc.                                    **
 * @link        http://www.csj.ualberta.ca                                **
 * @author      Patrick Thibaudeau                                        **
 * @author      Nicolas Bretin                                            **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once('locallib.php');

/**
 * Display the content of the page
 * @global stdobject $CFG
 * @global moodle_database $DB
 * @global core_renderer $OUTPUT
 * @global moodle_page $PAGE
 * @global stdobject $SESSION
 * @global stdobject $USER
 */
function display_page() {
    // CHECK And PREPARE DATA
    global $CFG, $OUTPUT, $SESSION, $PAGE, $DB, $USER;

    $cmid = required_param('cmid', PARAM_INT);
    $id = required_param('id', PARAM_INT);

    $cm = get_coursemodule_from_id('ddfill', $cmid);
    $course = $DB->get_record('course', array('id' => $cm->course));
    require_login($course, true, $cm);
    $context = context_module::instance($cm->id);

    require_capability('mod/ddfill:edit', $context);

    $delete = $DB->delete_records('ddfill_sentences', array('id' => $id));
    redirect('sentences.php?cmid='.$cmid);
}

display_page();

