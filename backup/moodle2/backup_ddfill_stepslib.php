<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package moodlecore
 * @subpackage backup-moodle2
 * @copyright 2010 onwards Eloy Lafuente (stronk7) {@link http://stronk7.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the backup steps that will be used by the backup_ddfill_activity_task
 */

/**
 * Define the complete ddfill structure for backup, with file and id annotations
 */
class backup_ddfill_activity_structure_step extends backup_activity_structure_step {

    protected function define_structure() {

        // To know if we are including userinfo
        $userinfo = $this->get_setting_value('userinfo');

        // Define each element separated
        $ddfill = new backup_nested_element('ddfill', array('id'), array(
            'name', 'intro', 'introformat','timecreated','timemodified'));

        $sentences = new backup_nested_element('sentences');

        $sentence = new backup_nested_element('sentence', array('id'), array(
            'ddfillid', 'sentenceleft', 'sentenceright', 'answer', 'answerlist'));

        // Build the tree
        $ddfill->add_child($sentences);
        $sentences->add_child($sentence);

        // Define sources
        $ddfill->set_source_table('ddfill', array('id' => backup::VAR_ACTIVITYID));

        $sentence->set_source_table('ddfill_sentences', array('ddfillid' => backup::VAR_PARENTID), 'id ASC');


        // Define id annotations
        $sentence->annotate_ids('ddfillid', 'id');

        // Define file annotations
        $ddfill->annotate_files('mod_ddfill', 'intro', null); // This file area hasn't itemid

        // Return the root element (ddfill), wrapped into standard activity structure
        return $this->prepare_activity_structure($ddfill);
    }
}
