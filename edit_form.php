<?php
/**
 * *************************************************************************
 * *                   Drag & Dop Fill in the blank                       **
 * *************************************************************************
 * @package     mod                                                       **
 * @subpackage  ddfill                                                    **
 * @name        ddfill                                                    **
 * @copyright   Oohoo It Services Inc.                                    **
 * @link        http://www.csj.ualberta.ca                                **
 * @author      Patrick Thibaudeau                                        **
 * @author      Nicolas Bretin                                            **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */

defined('MOODLE_INTERNAL') || die();

require_once("$CFG->dirroot/lib/formslib.php");

/**
 * Module instance settings form
 */
class ddfill_edit_form extends moodleform {

    function definition() {

        global $CFG, $USER, $DB;
        $sentence = $this->_customdata['sentence'];
        $mform = & $this->_form;
        $mform->addElement('text', 'sentenceleft', get_string('sentenceleft', 'ddfill'), array('size' => '150px')); // Add elements to your form
        $mform->setType('sentenceleft', PARAM_TEXT);                   //Set type of element$mform->addElement('text', 'answer', get_string('answer', ' ddfill')); // Add elements to your form
        $mform->addElement('text', 'answer', get_string('answer', 'ddfill'));
        $mform->setType('answer', PARAM_TEXT);                   //Set type of element
        $mform->addElement('text', 'sentenceright', get_string('sentenceright', 'ddfill'), array('size' => '150px')); // Add elements to your form
        $mform->setType('sentenceright', PARAM_TEXT);                   //Set type of element
        $mform->addElement('textarea', 'answerlist', get_string('answerlist', 'ddfill'), array('cols' => '50', 'rows' => '5')); // Add elements to your form
        $mform->setType('answerlist', PARAM_TEXT); 
        
        $mform->addElement('hidden', 'cmid'); 
        $mform->setType('cmid', PARAM_INT);
        $mform->addElement('hidden', 'id'); 
        $mform->setType('id', PARAM_INT);
        
//-------------------------------------------------------------------------------
// add standard buttons, common to all modules
        $this->add_action_buttons();

// set the defaults
        $this->set_data($sentence);
    }

}