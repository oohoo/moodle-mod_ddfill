Drag and Drop fill in the blank
===============================

This is a simple Moodle activity that allows teachers to prepare sentences with a blank option. The teacher also
creates a list of words for the students to chose from. Students can drag and drop the appropriate word in the blank space.
If the correct word was chosen, the word is highlighted in green and replaces the blank space. If it is the wrong
word, the word shakes, turns red and returns into the list.

There is a scoreboard that follows on the right side of the page. It basically displays the number of attempts against the number of questions.
For example, if there are 4 questions and the student took 6 attempts to complete all questions, the scoreboard will
show 6/4

Note that in this first version, there is no support for the grade book. Student attempts are not stored either.
This was designed for practicing only.

# Installation

1. Simply decompress into the Moodle_root/mod/ folder.
2. Rename the folder ddfill
3. Login to your Moodle site as administrator and follow the steps


 
