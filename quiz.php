<?php

/**
 * *************************************************************************
 * *                   Drag & Dop Fill in the blank                       **
 * *************************************************************************
 * @package     mod                                                       **
 * @subpackage  ddfill                                                    **
 * @name        ddfill                                                    **
 * @copyright   Oohoo It Services Inc.                                    **
 * @link        http://www.csj.ualberta.ca                                **
 * @author      Patrick Thibaudeau                                        **
 * @author      Nicolas Bretin                                            **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once('locallib.php');

/**
 * Display the content of the page
 * @global stdobject $CFG
 * @global moodle_database $DB
 * @global core_renderer $OUTPUT
 * @global moodle_page $PAGE
 * @global stdobject $SESSION
 * @global stdobject $USER
 */
function display_page() {
    // CHECK And PREPARE DATA
    global $CFG, $OUTPUT, $SESSION, $PAGE, $DB, $USER;

    $cmid = required_param('cmid', PARAM_INT);

    $cm = get_coursemodule_from_id('ddfill', $cmid);
    $course = $DB->get_record('course', array('id' => $cm->course));
    require_login($course, true, $cm);
    $context = context_module::instance($cm->id);
    $ddfill = $DB->get_record('ddfill', array('id' => $cm->instance));
    add_to_log($course->id, 'ddfill', 'view', "quiz.php?cmid={$cm->id}", $ddfill->name, $cm->id);
    require_capability('mod/ddfill:view', $context);

    $sentences = $DB->get_records('ddfill_sentences', array('ddfillid' => $cm->instance));

    ddfill_page($CFG->pluginlocalwww . '/quiz.php', get_string('pluginname', 'ddfill'), get_string('quiz', 'ddfill'), $context);
    //--------------------------------------------------------------------------
    echo $OUTPUT->header();
    //**********************
    //*** DISPLAY HEADER ***
    //Content
    $initjs = '$(document).ready(function() {
                        $("#ddfill-stats").scrollToFixed({ marginTop: 75 });;
                    });';
    echo html_writer::script($initjs);
    $i = 0;
    $count = count($sentences);
    $html = '<div id="ddfill-stats"class="ui-state-highlight">' ."\n";
    $html .= get_string('score', 'ddfill')."\n";
    $html .= '<div id="total"></div>'
            . '</div>';
    $html .= "<input type='hidden' value='$count' id='count'>";
    $html .= "<input type='hidden' value='0' id='attempts'>";
    foreach($sentences as $s) {
    $html .= "<div id='sentences_wrapper$i' class='ui-state-highlight sentences_wrapper'>"." \n";
    //Convert answerlist into array
    $answers = explode(',', $s->answerlist);
    //encode for javascript
    $answerlist = json_encode($answers);
    $initjs = '$(document).ready(function() {
                        initQuiz(\''. $i .'\','. $answerlist . ',\''. $s->answer . '\',\'' . $s->id . '\');
                    });';
    echo html_writer::script($initjs);
    //$html .= $i +1 . " Drag and drop from the list below";
    $html .= "	<div id='possible-answers$i' class='possible-answers' ></div>"." \n";
    $html .= "      <div id='answer-container$i' class='answer-container'>"." \n";
    $html .= "          <div id='sentence-begin$i' class='sentence-begin'>$s->sentenceleft</div>"." \n";
    $html .= "          <div id='answer$i' class='answer'>" . get_string('draghere', 'ddfill') . "</div>"." \n";
    $html .= "          <div id='sentence-end$i' class='sentence-end'>$s->sentenceright</div>"." \n";
    $html .= "      </div>"." \n";
    $html .= "</div>";
    $i++;
    }
    if (has_capability('mod/ddfill:edit', $context)){
        $html .= "<a id='return' href='sentences.php?cmid=$cmid'>" . get_string('return', 'ddfill') . "</a>";
    }
    echo $html;
    //**********************
    //*** DISPLAY FOOTER ***
    //**********************
    echo $OUTPUT->footer();
}

display_page();

