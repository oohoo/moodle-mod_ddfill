<?php
/**
 * *************************************************************************
 * *                   Drag & Dop Fill in the blank                       **
 * *************************************************************************
 * @package     mod                                                       **
 * @subpackage  ddfill                                                    **
 * @name        ddfill                                                    **
 * @copyright   Oohoo It Services Inc.                                    **
 * @link        http://www.csj.ualberta.ca                                **
 * @author      Patrick Thibaudeau                                        **
 * @author      Nicolas Bretin                                            **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once('locallib.php');
require_once('edit_form.php');


/**
 * Display the content of the page
 * @global stdobject $CFG
 * @global moodle_database $DB
 * @global core_renderer $OUTPUT
 * @global moodle_page $PAGE
 * @global stdobject $SESSION
 * @global stdobject $USER
 */
function display_page() {
    // CHECK And PREPARE DATA
    global $CFG, $OUTPUT, $SESSION, $PAGE, $DB, $USER;

    $cmid = required_param('cmid', PARAM_INT);
    $id = optional_param('id', 0, PARAM_INT);

    $cm = get_coursemodule_from_id('ddfill', $cmid);
    $course = $DB->get_record('course', array('id' => $cm->course));
    require_login($course, true, $cm);
    $context = context_module::instance($cm->id);

    require_capability('mod/ddfill:edit', $context);
    if($id != 0) {
        $sentence = $DB->get_record('ddfill_sentences', array('id' => $id), '*', MUST_EXIST);
        $sentence->cmid = $cmid;
    } else {
        $sentence = new stdClass();
        $sentence->id = 0;
        $sentence->cmid = $cmid;
    }
    
    $mform = new ddfill_edit_form(null, array('sentence' => $sentence));

// If data submitted, then process and store.
    if ($mform->is_cancelled()) {
        redirect('sentences.php?cmid='.$cmid);
    } else if ($data = $mform->get_data()) {
        if ($data->id) {
            
            $DB->update_record('ddfill_sentences', $data);
            
        } else {
            $data->ddfillid = $cm->instance;
            $DB->insert_record('ddfill_sentences', $data);
        }
        redirect('sentences.php?cmid='.$cmid);
    }



    ddfill_page($CFG->pluginlocalwww . '/iedit.php', get_string('pluginname', 'ddfill'), get_string('configuration', 'ddfill'), $context);
    //--------------------------------------------------------------------------
    echo $OUTPUT->header();
    //**********************
    //*** DISPLAY HEADER ***
    $initjs = "$(document).ready(function() {
                        init();
                    });";
    echo html_writer::script($initjs);

    $mform->display();
    //**********************
    //*** DISPLAY FOOTER ***
    //**********************
    echo $OUTPUT->footer();
}

display_page();
?>

