<?php
/**
 * *************************************************************************
 * *                   Drag & Dop Fill in the blank                       **
 * *************************************************************************
 * @package     mod                                                       **
 * @subpackage  ddfill                                                    **
 * @name        ddfill                                                    **
 * @copyright   Oohoo It Services Inc.                                    **
 * @link        http://www.csj.ualberta.ca                                **
 * @author      Patrick Thibaudeau                                        **
 * @author      Nicolas Bretin                                            **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
defined('MOODLE_INTERNAL') || die();

$string['actions'] = 'Actions';
$string['answer'] = 'Answer';
$string['answer_help'] = 'Enter the correct answer';
$string['answerlist'] = 'Answer list';
$string['answerlist_help'] = 'Enter a list of words (answers) seerated by commas. Remember to include the correct answer.';
$string['btn_delete'] = 'Delete';
$string['create_sentence'] = 'Create sentence';
$string['configuration'] = 'Sentence configuration';
$string['confirm_delete'] = 'Are you sure you want to delete this record. This procedure cannot be undone.';
$string['modulename'] = 'Drag & Drop fill in the blank';
$string['modulenameplural'] = 'Drag & Drop fill in the blanks';
$string['modulename_help'] = 'Use the newmodule module for... | The newmodule module allows...';
$string['ddfillfieldset'] = 'Custom example fieldset';
$string['ddfillname'] = 'Drag & Drop fill in the blank';
$string['ddfillname_help'] = 'This is the content of the help tooltip associated with the newmodulename field. Markdown syntax is supported.';
$string['ddfill'] = 'Drag & Drop fill in the blank';
$string['draghere'] = 'Drag answer here';
$string['pluginadministration'] = 'Drag & Drop fill in the blank administration';
$string['pluginname'] = 'Drag & Drop fill in the blank';
$string['preview_quiz'] = 'Preview';
$string['quiz'] = 'Quiz';
$string['return'] = 'Return';
$string['score'] = 'Your score';
$string['sentenceleft'] = 'Sentence: left portion';
$string['sentenceleft_help'] = 'Enter the left portion of the sentence';
$string['sentenceright'] = 'Sentence: right portion';
$string['sentenceright_right'] = 'Enter the right portion (what follows the answer) of the sentence';

