<?php
/**
 * *************************************************************************
 * *                   Drag & Dop Fill in the blank                       **
 * *************************************************************************
 * @package     mod                                                       **
 * @subpackage  ddfill                                                    **
 * @name        ddfill                                                    **
 * @copyright   Oohoo It Services Inc.                                    **
 * @link        http://www.csj.ualberta.ca                                **
 * @author      Patrick Thibaudeau                                        **
 * @author      Nicolas Bretin                                            **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
defined('MOODLE_INTERNAL') || die();

$string['actions'] = 'Actions';
$string['answer'] = 'Réponse';
$string['answer_help'] = 'Entrez la bonne réponse';
$string['answerlist'] = 'Liste de réponse';
$string['answerlist_help'] = 'Entrez une list de mots (réponse) séparer par des vergule. N\'oublié pas d\'inclure la bonne réponse.';
$string['btn_delete'] = 'Effacer';
$string['create_sentence'] = 'Ajouter une phrase';
$string['configuration'] = 'Configuration de les phrases';
$string['confirm_delete'] = 'Êtes vous certain de vouloir effacer cette phrase?';
$string['modulename'] = 'Glisser-déplacer champs vide';
$string['modulenameplural'] = 'Glisser-déplacer champs vides';
$string['modulename_help'] = 'Use the newmodule module for... | The newmodule module allows...';
$string['ddfillname'] = 'Glisser-déplacer champs vide';
$string['ddfillname_help'] = 'This is the content of the help tooltip associated with the newmodulename field. Markdown syntax is supported.';
$string['ddfill'] = 'Glisser-déplacer champs vide';
$string['draghere'] = 'Glisser la réponse ici';
$string['pluginadministration'] = 'Administration Glisser-déplacer champs vide';
$string['pluginname'] = 'Glisser-déplacer champs vide';
$string['preview_quiz'] = 'Visionner ';
$string['quiz'] = 'Quiz';
$string['return'] = 'Retour';
$string['score'] = 'Votre score';
$string['sentenceleft'] = 'Phrase : partie gauche';
$string['sentenceleft_help'] = 'Entrez la portion gauche de la phrase';
$string['sentenceright'] = 'Phrase : partie droite';
$string['sentenceright_right'] = 'Saisissez la partie droite (ce qui découle de la réponse) de la phrase ';

