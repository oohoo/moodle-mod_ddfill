<?php
/**
 * *************************************************************************
 * *                   Drag & Dop Fill in the blank                       **
 * *************************************************************************
 * @package     mod                                                       **
 * @subpackage  ddfill                                                    **
 * @name        ddfill                                                    **
 * @copyright   Oohoo It Services Inc.                                    **
 * @link        http://www.csj.ualberta.ca                                **
 * @author      Patrick Thibaudeau                                        **
 * @author      Nicolas Bretin                                            **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */

defined('MOODLE_INTERNAL') || die();

/**
 * Execute ddfill upgrade from the given old version
 *
 * @param int $oldversion
 * @return bool
 */
function xmldb_ddfill_upgrade($oldversion) {
    global $DB;

    $dbman = $DB->get_manager(); // loads ddl manager and xmldb classes

        if ($oldversion < 2014020501) {

        // Define table ddfill_sentences to be created.
        $table = new xmldb_table('ddfill_sentences');

        // Adding fields to table ddfill_sentences.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('ddfillid', XMLDB_TYPE_INTEGER, '20', null, null, null, null);
        $table->add_field('sentenceleft', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('sentenceright', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('answer', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('answerlist', XMLDB_TYPE_TEXT, null, null, null, null, null);

        // Adding keys to table ddfill_sentences.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for ddfill_sentences.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Ddfill savepoint reached.
        upgrade_mod_savepoint(true, 2014020501, 'ddfill');
    }
    
    $dbman = $DB->get_manager(); // loads ddl manager and xmldb classes

        if ($oldversion < 2014020700) {

        // Define table ddfill_sentences to be created.
        $table = new xmldb_table('ddfill_sentences');

        // Adding fields to table ddfill_sentences.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('ddfillid', XMLDB_TYPE_INTEGER, '20', null, null, null, null);
        $table->add_field('sentenceleft', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('sentenceright', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('answer', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('answerlist', XMLDB_TYPE_TEXT, null, null, null, null, null);

        // Adding keys to table ddfill_sentences.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for ddfill_sentences.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Ddfill savepoint reached.
        upgrade_mod_savepoint(true, 2014020700, 'ddfill');
    }

    return true;
}
