<?php
/**
 * *************************************************************************
 * *                   Drag & Dop Fill in the blank                       **
 * *************************************************************************
 * @package     mod                                                       **
 * @subpackage  ddfill                                                    **
 * @name        ddfill                                                    **
 * @copyright   Oohoo It Services Inc.                                    **
 * @link        http://www.csj.ualberta.ca                                **
 * @author      Patrick Thibaudeau                                        **
 * @author      Nicolas Bretin                                            **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
require_once(dirname(__FILE__) . '../../../config.php');


defined('MOODLE_INTERNAL') || die();
global $DB;
$CFG->pluginlocal = 'ddfill';
$CFG->pluginlocalwww = $CFG->wwwroot . '/mod/' . $CFG->pluginlocal;
$CFG->pluginlocalstyle = '/mod/' . $CFG->pluginlocal . '/css/';
$CFG->pluginlocalscript = '/mod/' . $CFG->pluginlocal . '/js/';
?>